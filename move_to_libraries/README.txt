The Pirobox module uses a modified version of the Pirobox jQuery plugin
(not the extended version).

Move all content from the module folder pirobox/move_to_libraries/pirobox
to the folder sites/all/libraries/pirobox

The libraries folder structure after the moving:

  sites/all/libraries/pirobox/css
  sites/all/libraries/pirobox/images-background
  sites/all/libraries/pirobox/js

  The three folder css, images-background and js contains
  the Pirobox jQuery plugin.

The Pirobox module uses the Blur gaussian jQuery plugin.

Move all content from the module folder pirobox/move_to_libraries/blur-gaussian
to the folder sites/all/libraries/blur-gaussian

The libraries folder structure after the moving:

  sites/all/libraries/blur-gaussian/blur-gaussian.js
  sites/all/libraries/blur-gaussian/blur-gaussian.min.js
