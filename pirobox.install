<?php
/**
 * @file
 * Requirements, install, update and uninstall functions for the Pirobox module.
 */

/**
 * Implements hook_requirements().
 */
function pirobox_requirements($phase) {
  $requirements = array();

  if ($phase == 'runtime') {
    $t = get_t();
    // Pirobox
    $pirobox_library_path = pirobox_get_library_path('pirobox');
    $pirobox_version = pirobox_get_version('pirobox');
    $title = $t('Pirobox jQuery plugin');
    if (version_compare($pirobox_version, PIROBOX_MIN_PLUGIN_VERSION, '>=')) {
      $requirements['pirobox_plugin'] = array(
        'title' => $title,
        'severity' => REQUIREMENT_OK,
        'value' => $pirobox_version
      );
    }
    else {
      $requirements['pirobox_plugin'] = array(
        'title' => $title,
        'value' => $t('At least @a', array('@a' => PIROBOX_MIN_PLUGIN_VERSION)),
        'severity' => REQUIREMENT_ERROR,
        'description' => $t('Move the complete plugin content from the Pirobox module folder <em>move_to_libraries/pirobox</em> in the %path folder.', array('%path' => $pirobox_library_path))
      );
    }
    // Blur gaussian
    $blur_gaussian_library_path = pirobox_get_library_path('blur-gaussian');
    $pirobox_version = pirobox_get_version('blur-gaussian');
    $title = $t('Blur gaussian jQuery plugin');
    if (version_compare($pirobox_version, BLUR_GAUSSIAN_MIN_PLUGIN_VERSION, '>=')) {
      $requirements['blur_gaussian_plugin'] = array(
        'title' => $title,
        'severity' => REQUIREMENT_OK,
        'value' => $pirobox_version
      );
    }
    else {
      $requirements['blur_gaussian_plugin'] = array(
        'title' => $title,
        'value' => $t('At least @a', array('@a' => BLUR_GAUSSIAN_MIN_PLUGIN_VERSION)),
        'severity' => REQUIREMENT_ERROR,
        'description' => $t('Move the complete plugin content from the Pirobox module folder <em>move_to_libraries/blur-gaussian</em> in the %path folder.', array('%path' => $blur_gaussian_library_path))
      );
    }
  }

  return $requirements;
}

/**
 * Implementation of hook_uninstall().
 */
function pirobox_uninstall() {
  $results = db_select('variable', 'v')->fields('v', array('name'))->condition('name', '%pirobox_%', 'LIKE')->condition('name', '%pirobox_limit_%', 'NOT LIKE')->execute()->fetchAll();
  foreach ($results as $result) {
    variable_del($result->name);
  }
}
