DO NOT USE this folder to place the Pirobox jQuery plugin JS files!

Place the Pirobox and Blur gaussian jQuery plugin JS files
to the folders
sites/all/libraries/pirobox/js
sites/all/libraries/blur-gaussian
Create the Pirobox and Blur gaussian library folder if it does not exist.

Ergo:
sites/all/libraries/pirobox/js/pirobox.js
sites/all/libraries/pirobox/js/pirobox.min.js

sites/all/libraries/blur-gaussian/blur-gaussian.js
sites/all/libraries/blur-gaussian/blur-gaussian.min.js
