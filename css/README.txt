This directory should NOT be used to place the Pirobox jQery plugin CSS files.

Put all  CSS content of Pirobox jQuery plugin to the folder
sites/all/libraries/pirobox/css.
Create the Pirobox library folder if it does not exist.

Ergo:

  sites/all/libraries/pirobox/css/demo1/
  sites/all/libraries/pirobox/css/demo2/
  sites/all/libraries/pirobox/css/demo3/
  sites/all/libraries/pirobox/css/demo4/
  sites/all/libraries/pirobox/css/demo5/
  sites/all/libraries/pirobox/css/Fancy_1/
