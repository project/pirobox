Pirobox Media provides Media module support for the Pirobox module.

This module is an submodule of Pirobox.

REQUIREMENTS
------------
Drupal 7.8 or higher.
Media and Media field module version 2.
Pirobox module.

ADMINISTRATION
--------------
Pirobox Media does not have its own administration to configure settings.
Pirobox Media does not have its own permission settings.


LIMITATIONS
------------
The Pirobox module supports only images.

  Please note that in the configuration of allowed extensions of a media field 
  which uses the Pirobox.
