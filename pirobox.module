<?php
/**
 * @file
 * Enables the use of Pirobox.
 *
 * Relative paths problem
 *   For a solution it is possible to use mode_rewrite.
 *   Take a look at http://drupal.org/node/84895#comment-1319154
 */

/**
 * Default path to the Pirobox plugin library directory.
 */
define('PIROBOX_PATH', 'sites/all/libraries/pirobox');

/**
 * Default path to the Blur gaussian plugin library directory.
 */
define('BLUR_GAUSSIAN_PATH', 'sites/all/libraries/blur-gaussian');

/**
 * Pirobox plugin version. The last digit reflects the Drupal version.
 */
define('PIROBOX_MIN_PLUGIN_VERSION', '1.2.57');

/**
 * Blur gaussian plugin version. Last digit reflects Drupal version.
 */
define('BLUR_GAUSSIAN_MIN_PLUGIN_VERSION', '1.1.17');

/**
 * Implements hook_permission().
 */
function pirobox_permission() {
  return array(
    'administer pirobox' => array(
      'title' => t('Administer Pirobox settings')
    )
  );
}

/**
 * Implements hook_init().
 */
function pirobox_init() {
  // Don't add the JavaScript and CSS on specified paths.
  if (!_pirobox_active()) {
    return;
  }

  $module_path = drupal_get_path('module', 'pirobox');
  // Pirobox jQuery plugin.
  $pirobox_library_path = pirobox_get_library_path('pirobox');
  // Blur gaussian jQuery plugin.
  $blur_gaussian_library_path = pirobox_get_library_path('blur-gaussian');

  // Insert options and translated strings as Pirobox module JS settings.
  $js_settings = array(
    'style' => variable_get('pirobox_style', 'demo1'),
    'overlayColor' => variable_get('pirobox_overlay_color', 'style'),
    // Ensure that bg overlay images works in a subfolder installation.
    'overlayBgImage' => substr(base_path(), 1) . variable_get('pirobox_overlay_bgimage', 'white'),
    'overlayOpacity' => variable_get('pirobox_overlay_opacity', '0.60'),
    'animationSpeed' => variable_get('pirobox_animation_speed', 400),
    'overlayClose'   => variable_get('pirobox_overlayclose', 1),
    'slideshow'      => variable_get('pirobox_slideshow', FALSE),
    'slideshowSpeed' => variable_get('pirobox_slideshowspeed', 6),
    'linkToShow'     => variable_get('pirobox_linkto', 0),
    // Image effects.
    'imgEffect'      => variable_get('pirobox_image_effect', 'fadein'),
    'imgBlurRadius'  => variable_get('pirobox_blurgauss_factor', 2),
    'blurTime'       => variable_get('pirobox_blurgauss_time', 3000)
  );
  drupal_add_js(array('pirobox' => $js_settings), array('type' => 'setting', 'scope' => JS_DEFAULT));

  // Add Pirobox and Blur gaussian jQuery plugin JS.
  switch (variable_get('pirobox_compression_type', 'min')) {
    case 'min':
      drupal_add_js($pirobox_library_path . '/js/pirobox.min.js', array('scope' => 'footer'));
      drupal_add_js($blur_gaussian_library_path . '/blur-gaussian.min.js', array('scope' => 'footer'));
      break;
    case 'none':
      drupal_add_js($pirobox_library_path . '/js/pirobox.js', array('scope' => 'header'));
      drupal_add_js($blur_gaussian_library_path . '/blur-gaussian.js', array('scope' => 'header'));
      break;
  }
  // Add Pirobox module JS.
  switch (variable_get('pirobox_module_compression_type', 'min')) {
    case 'min':
      drupal_add_js($module_path . '/js/pirobox.module.min.js', array('scope' => 'footer'));
      break;
    case 'none':
      drupal_add_js($module_path . '/js/pirobox.module.js', array('scope' => 'footer'));
      break;
  }
  // Add Pirobox jQuery plugin CSS.
  drupal_add_css($pirobox_library_path . '/css/' . variable_get('pirobox_style', 'demo1') . '/piroboxstyle.css');
  // Add Pirobox module CSS.
  drupal_add_css($module_path . '/css/pirobox.module.css');
}

/**
 * Return the paths to the installed Pirobox jQuery plugins.
 *
 * @param $library
 *   String of the library to get the path. Possible values:
 *   - pirobox
 *   - blur-gaussian
 *
 * @return
 *   A string contains the library path.
 *
 * @see pirobox_init()
 */
function pirobox_get_library_path($library) {
  $library_path = NULL;

  // Try to locate the library path in any possible setup.
  if ($library_path == NULL) {
    switch ($library) {
      case 'pirobox':
        // First check the default location.
        $module_path = variable_get('pirobox_path', PIROBOX_PATH);
        if (is_dir($module_path . '/pirobox')) {
          $library_path = $module_path;
        }
        // Ask the libraries module as a fallback.
        elseif ($library_path == NULL && module_exists('libraries')) {
          if ($module_path = libraries_get_path('pirobox')) {
            $library_path = $module_path;
            variable_set('pirobox_path', $library_path);
          }
        }
        // HACK: If libraries api module is not enabled but available, load it.
        elseif ($library_path == NULL && file_exists(dirname(__FILE__) . '/../../libraries/libraries.module')) {
          require_once(dirname(__FILE__) . '/../../libraries/libraries.module');
          if ($module_path = libraries_get_path('pirobox')) {
            $library_path = $module_path;
            variable_set('pirobox_path', $library_path);
          }
        }
        // If no path is found suggest the default one.
        elseif ($library_path == NULL) {
          $library_path = PIROBOX_PATH;
        }
        break;

      case 'blur-gaussian':
        // First check the default location.
        $module_path = variable_get('blur_gaussian_path', BLUR_GAUSSIAN_PATH);
        if (is_dir($module_path . '/blur-gaussian')) {
          $library_path = $module_path;
        }
        // Ask the libraries module as a fallback.
        elseif ($library_path == NULL && module_exists('libraries')) {
          if ($module_path = libraries_get_path('blur-gaussian')) {
            $library_path = $module_path;
            variable_set('blur_gaussian_path', $library_path);
          }
        }
        // HACK: If libraries api module is not enabled but available, load it.
        elseif ($library_path == NULL && file_exists(dirname(__FILE__) . '/../../libraries/libraries.module')) {
          require_once(dirname(__FILE__) . '/../../libraries/libraries.module');
          if ($module_path = libraries_get_path('blur-gaussian')) {
            $library_path = $module_path;
            variable_set('blur_gaussian_path', $library_path);
          }
        }
        // If no path is found suggest the default one.
        elseif ($library_path == NULL) {
          $library_path = BLUR_GAUSSIAN_PATH;
        }
        break;
    }
  }

  return $library_path;
}

/**
 * Implements hook_menu().
 */
function pirobox_menu() {
  $items = array();

  $items['admin/config/media/pirobox'] = array(
    'title' => 'Pirobox',
    'page callback' => 'drupal_get_form',
    'page arguments' =>  array('pirobox_general_settings_form'),
    'access arguments' =>  array('administer pirobox'),
    'description' => 'Adjust the Pirobox lightbox settings.',
    'file' => 'includes/pirobox.admin.inc',
    'access arguments' => array('administer pirobox'),
    'weight' => -10
  );
  $items['admin/config/media/pirobox/settings'] = array(
    'title' => 'Settings',
    'description' => 'Adjust the Pirobox lightbox settings.',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10
  );

  return $items;
}

/**
 * Implements hook_form_alter().
 *
 * Insert JS files needed by formatter settings form.
 */
function pirobox_form_alter(&$form, &$form_state, $form_id) {
  // Views UI config item forms can't load JS files.
  // We use the parent form to make the files available.
  if ($form_id == 'field_ui_display_overview_form' || $form_id == 'views_ui_preview_form' || $form_id == 'views_ui_config_item_form') {
    $module_path = drupal_get_path('module', 'pirobox');
    // Attach to form element(s)
    // in hook_field_formatter_settings_form() not work.
    switch (variable_get('pirobox_module_compression_type', 'min')) {
      case 'min':
        drupal_add_js($module_path . '/js/pirobox.formatter.module.min.js', array('scope' => 'footer'));
        break;
      case 'none':
        drupal_add_js($module_path . '/js/pirobox.formatter.module.js', array('scope' => 'footer'));
        break;
    }
  }
  if ($form_id == 'views_ui_preview_form' || $form_id == 'views_ui_config_item_form') {
    $module_path = drupal_get_path('module', 'pirobox');
    switch (variable_get('pirobox_module_compression_type', 'min')) {
      case 'min':
        drupal_add_js($module_path . '/js/pirobox.views_formatter.module.min.js', array('scope' => 'footer'));
        break;
      case 'none':
        drupal_add_js($module_path . '/js/pirobox.views_formatter.module.js', array('scope' => 'footer'));
        break;
    }
  }
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 *
 * Add a #process callback to the imagefield widget element so we
 * can perform alters.
 */
function pirobox_field_widget_image_image_form_alter(&$element, &$form_state, $context) {
  if (count($context['items']) > 1) {
    $element[0]['#process'][] = 'pirobox_imagefield_widget_process';
  }
}

/**
 * Element #process callback; Provide a information if gallery covering active.
 *
 * @see pirobox_field_widget_image_image_form_alter()
 */
function pirobox_imagefield_widget_process($element, &$form_state, $form) {
  $instance = field_info_instance($element['#entity_type'], $element['#field_name'], $element['#bundle']);

  $extend = FALSE;
  foreach (element_children($instance['display']) as $display) {
    if (isset($instance['display'][$display]['settings']['pirobox_gallery_covering']) && $instance['display'][$display]['settings']['pirobox_gallery_covering'] == TRUE) {
      $extend = TRUE;
    }
  }

  if ($extend == TRUE && isset($element['filename'])) {
    $filename = $element['filename']['#markup'];
    $element['filename']['#markup'] = '<div class="description pirobox-description">' . _pirobox_get_covering_message() . '</div>' . $filename;
  }

  // Needed by alter hook.
  $vars['form_state'] = $form_state;
  $vars['form'] = $form;
  $vars['instance'] = $instance;

  // Allow other modules to alter the widget element.
  drupal_alter('pirobox_imagefield_widget_process', $element, $vars);

  return $element;
}

/**
 * Implements hook_field_formatter_info().
 */
function pirobox_field_formatter_info() {
  $formatters = array(
    'pirobox' => array(
      'label' => t('Pirobox'),
      'field types' => array('image'),
      'settings' => pirobox_field_formatter_get_settings()
    )
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function pirobox_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $element = pirobox_field_formatter_settings_form_get_element($field, $instance, $view_mode, $form, $form_state);

  return $element;
}

/**
 * Returns the form elements for hook_field_formatter_settings_form().
 *
 * @return
 *   The associative array contains the form elements for the formatter
 *   settings form.
 *
 * @see pirobox_field_formatter_settings_form()
 */
function pirobox_field_formatter_settings_form_get_element($field, $instance, $view_mode, $form, $form_state) {
  $display = $instance['display'][$view_mode];

  $settings = $display['settings'];
  $description_caption = '';

  $image_styles = pirobox_image_style_options(FALSE);

  $extend_styles = array(
    'hide' => t('Hide (do not display image)'),
    'link' => t('Link to image')
  );

  // Needed by alter hook.
  $vars['field'] = $field;
  $vars['instance'] = $instance;
  $vars['view_mode'] = $view_mode;
  $vars['form'] = $form;
  $vars['display'] = $display;
  $vars['image_styles'] = $image_styles;
  $vars['extend_styles'] = $extend_styles;

  $module_path = drupal_get_path('module', 'pirobox');

  // Get different form element states.
  //
  // A different states property of form elements
  // is necessary if the form called from
  // Views UI config item forms.
  $element_states = array();
  if (!isset($form['#entity_type']) && isset($form_state['view'])) {
    // Caller is a views form.
    $element_states = _pirobox_formatter_settings_form_get_states('views');
    $vars['form_caller'] = 'views';
  }
  else {
    $element_states = _pirobox_formatter_settings_form_get_states('default');
    $vars['form_caller'] = 'default';
  }

  // Content image style
  $element['pirobox_entity_style'] = array(
    '#type' => 'select',
    '#title' => t('Content image style'),
    '#description' => t('Image style to use in the content.'),
    '#default_value' => $settings['pirobox_entity_style'],
    '#empty_option' => t('None (original image)'),
    '#options' => array_merge($extend_styles, $image_styles),
    '#attributes' => array(
      'class' => array('pirobox-entity-style')
    ),
    '#weight' => -10
  );

  // Pirobox image style
  $element['pirobox_image_style'] = array(
    '#type' => 'select',
    '#title' => t('Pirobox image style'),
    '#description' => t('Select which image style to use for viewing images in the Pirobox.'),
    '#default_value' => $settings['pirobox_image_style'],
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
    '#attributes' => array(
      'class' => array('pirobox-image-style')
    ),
    '#weight' => -8
  );

  // Use entity types to provide different formatter setting values for
  // node, comment and taxonomy term content.
  $entity_type = $instance['entity_type'];

  // Ensure proper entity type value for the Views module.
  //
  // If the field_formatter_settings_form called from a Views display then the
  // entity type in $instance['entity_type'] is changed to ctools!
  if ($entity_type == 'ctools' && !isset($form['#entity_type']) && isset($form_state['view'])) {
    $entity_type = _pirobox_get_entity_type_from_view($form_state['view'], $field);
  }

  $vars['entity_type'] = $entity_type;

  // Entity type node.
  if ($entity_type == 'node') {
    $gallery = array(
      'post' => t('Per post gallery'),
      'page' => t('Per page gallery'),
      'field_post' => t('Per field in post gallery'),
      'field_page' => t('Per field in page gallery'),
      'post_user' => t('Per post per user gallery'),
      'page_user' => t('Per page per user gallery')
    );
    $description_caption = t('Automatic will use the first none empty value of the title, the alt text and the node title.');
  }
  // Entity type comment.
  if ($entity_type == 'comment') {
    $gallery = array(
      'comment' => t('Per comment gallery'),
      'page' => t('Per page gallery')
    );
    $description_caption = t('Automatic will use the first none empty value of the title, the alt text and the comment subject.');
  }
  // Entity type taxonomy term.
  if ($entity_type == 'taxonomy_term') {
    $gallery = array(
      'term' => t('Per taxonomy term gallery'),
      'page' => t('Per page gallery')
    );
    $description_caption = t('Automatic will use the first none empty value of the title, the alt text and the term name.');
  }

  // All entity types or no proper entity type found.
  $gallery['custom'] = t('Custom');
  $gallery['none'] = t('No gallery');

  $element['pirobox_gallery_grouping'] = array(
    '#type' => 'select',
    '#title' => t('Gallery (image grouping)'),
    '#description' => t('How Pirobox should group the image galleries.'),
    '#default_value' => $settings['pirobox_gallery_grouping'],
    '#options' => $gallery,
    '#attributes' => array(
      'class' => array('pirobox-gallery-grouping')
    ),
    '#weight' => -6
  );

  $element['pirobox_gallery_custom'] = array(
    '#type' => 'machine_name',
    '#title' => t('Custom gallery'),
    '#description' => t('All images on a page with the same gallery value (class attribute) will be grouped together. It must only contain lowercase letters, numbers, and underscores.'),
    '#size' => 32,
    '#maxlength' => 32,
    '#default_value' => check_plain($settings['pirobox_gallery_custom']),
    '#attributes' => array(
      'class' => array('pirobox-gallery-custom')
    ),
    '#required' => FALSE,
    '#machine_name' => array(
      'exists' => 'pirobox_gallery_exists',
      'error' => t('The custom gallery field must only contain lowercase letters, numbers, and underscores.')
    ),
    '#weight' => -4,
    '#element_validate' => array('pirobox_formatter_settings_validate_gallery_custom'),
    '#prefix' => '<div id="pirobox-gallery-custom-error" class="messages error" style="display: none;">' . t('The field <em>Custom gallery</em> is required.') . '</div>'
  );

  $element['pirobox_gallery_random'] = array(
    '#type' => 'checkbox',
    '#title' => t('Gallery image random'),
    '#description' => t('Check this option if you wish a random display in the gallery.'),
    '#default_value' => $settings['pirobox_gallery_random'],
    '#attributes' => array(
      'class' => array('pirobox-gallery-random')
    ),
    '#weight' => -3
  );
  $element['pirobox_gallery_covering'] = array(
    '#type' => 'select',
    '#title' => t('Gallery covering'),
    '#description' => t('Show at grouped images only the first image by post.'),
    '#default_value' => $settings['pirobox_gallery_covering'],
    '#options' => array(
      FALSE => t('No'),
      TRUE => t('Yes')
    ),
    '#attributes' => array(
      'class' => array('pirobox-gallery-covering')
    ),
    '#weight' => -2
  );

  $element['pirobox_caption'] = array(
    '#type' => 'select',
    '#title' => t('Caption'),
    '#description' => $description_caption,
    '#default_value' => $settings['pirobox_caption'],
    '#options' => _pirobox_caption($instance['entity_type']),
    '#attributes' => array(
      'class' => array('pirobox-caption')
    ),
    '#weight' => 0,
    '#states' => $element_states['pirobox_caption']
  );

  // Allow other modules to alter formatter settings form element.
  drupal_alter('pirobox_field_formatter_settings_form_get_element', $element, $vars);

  return $element;
}

/**
 * Element validation handler for pirobox_field_formatter_settings_form_get_element().
 *
 * @see pirobox_field_formatter_settings_form_get_element()
 */
function pirobox_formatter_settings_validate_gallery_custom($element, &$form_state, $form) {
  if (!isset($form_state['values']['fields'])) {
    return;
  }

  $element_title = t('!element-title', array('!element-title' => $element['#title']));

  $field_name = preg_replace("/fields\[/", '', $element['#name']);
  $field_name = preg_replace("/\].*/", '', $field_name);

  $other_values = $form_state['values']['fields'][$field_name]['settings_edit_form']['settings'];

  if ($other_values['pirobox_gallery_grouping'] == 'custom' && $other_values['pirobox_entity_style'] != 'hide' && empty($element['#value'])) {
    form_error($element, t('The field %field-title is required.', array('%field-title' => $element_title )));
  }
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function pirobox_field_formatter_settings_summary($field, $instance, $view_mode) {
  $summary = pirobox_field_formatter_settings_summary_get_summary($field, $instance, $view_mode);

  return implode('<br />', $summary);
}

/**
 * Returns the settings summary for hook_field_formatter_settings_summary().
 *
 * @return
 *   The array contains the formatter settings.
 *
 * @see pirobox_field_formatter_settings_summary()
 */
function pirobox_field_formatter_settings_summary_get_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $caption = _pirobox_caption($instance['entity_type']);

  $summary = array();

  $image_styles = pirobox_image_style_options(FALSE);

  // Needed by alter hook.
  $vars['field'] = $field;
  $vars['instance'] = $instance;
  $vars['view_mode'] = $view_mode;
  $vars['display'] = $display;
  $vars['settings'] = $settings;
  $vars['image_styles'] = $image_styles;

  // Styles could be lost because of enabled/disabled modules that defines
  // their styles in code.
  if (isset($image_styles[$settings['pirobox_entity_style']])) {
    $summary[] = t('Content image style: !style', array('!style' => $image_styles[$settings['pirobox_entity_style']]));
  }
  elseif ($settings['pirobox_entity_style'] == 'hide') {
    $summary[] = t('Content image style: Hide');

    return $summary;
  }
  elseif ($settings['pirobox_entity_style'] == 'link') {
    $summary[] = t('Content image style: Link to image');
  }
  else {
    $summary[] = t('Content image style: Original image');
  }

  if (isset($image_styles[$settings['pirobox_image_style']])) {
    $summary[] = t('Pirobox image style: !style', array('!style' => $image_styles[$settings['pirobox_image_style']]));
  }
  else {
    $summary[] = t('Pirobox image style: Original image');
  }

  switch ($instance['entity_type']) {
    case 'node':
      $gallery = array(
        'post' => t('Per post gallery'),
        'page' => t('Per page gallery'),
        'field_post' => t('Per field in post gallery'),
        'field_page' => t('Per field in page gallery'),
        'post_user' => t('Per post per user gallery'),
        'page_user' => t('Per page per user gallery'),
        'custom' => t('Custom'),
        'none' => t('No gallery')
      );
      break;
    case 'comment':
      $gallery = array(
        'comment' => t('Per comment gallery'),
        'page' => t('Per page gallery')
      );
      break;
    case 'taxonomy_term':
      $gallery = array(
        'term' => t('Per taxonomy term gallery'),
        'page' => t('Per page gallery')
      );
      break;
  }

  $gallery['custom'] = t('Custom');
  $gallery['none'] = t('No gallery');

  if (isset($settings['pirobox_gallery_grouping'])) {
    $summary[] = t('Pirobox gallery type: @type', array('@type' => $gallery[$settings['pirobox_gallery_grouping']])) . ($settings['pirobox_gallery_grouping'] == 'custom' ? ' (' . check_plain($settings['pirobox_gallery_custom']) . ')' : '');
  }
  if (isset($settings['pirobox_gallery_random'])) {
    $summary[] = t('Pirobox gallery random: @type', array('@type' => ($settings['pirobox_gallery_random'] == FALSE ? t('No') : t('Yes'))));
  }
  if (isset($settings['pirobox_gallery_covering'])) {
    $summary[] = t('Pirobox gallery covering: @type', array('@type' => ($settings['pirobox_gallery_covering'] == FALSE ? t('No') : t('Yes'))));
  }
  if (isset($settings['pirobox_caption'])) {
    $summary[] = t('Pirobox caption: @type', array('@type' => $caption[$settings['pirobox_caption']]));
  }

  // Allow other modules to alter the formatter settings summary content.
  drupal_alter('pirobox_field_formatter_settings_summary_get_summary', $summary, $vars);

  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 */
function pirobox_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  if ((bool) $display['settings']['pirobox_gallery_random']) {
    $items = _pirobox_randomize_assoc_array($items);
  }

  // Needed by the alter hooks.
  $vars['entity_type'] = $entity_type;
  $vars['entity'] = $entity;
  $vars['field'] = $field;
  $vars['instance'] = $instance;
  $vars['langcode'] = $langcode;
  $vars['items'] = $items;
  $vars['display'] = $display;

  // Allow other modules to alter the items.
  drupal_alter('pirobox_field_formatter_view', $items, $vars);

  $vars['items'] = $items;

  $i = 1;
  foreach ($items as $delta => $item) {
    if ($display['type'] == 'pirobox') {
      $element[$delta] = array(
        '#theme' => 'pirobox_formatter',
        '#item' => $item,
        '#entity_type' => $entity_type,
        '#entity' => $entity,
        '#field' => $field,
        '#display_settings' => $display['settings']
      );

      // Insert the requirement to use the gallery covering.
      //
      // See pirobox_field_formatter_settings_form_get_element()
      //     Makes gallery covering settings available.
      // See file pirobox.theme.inc|theme_pirobox_formatter()
      //     Use gallery covering settings
      if ($display['settings']['pirobox_gallery_covering'] == TRUE) {
        if ($i == 1) {
          $element[$delta]['#cover_class'] = ' gallerycover';
        }
        if ($i > 1) {
          $element[$delta]['#cover_class'] = ' gallerycover-no';
        }
      }

      ++$i;
    }
  }

  // Allow other modules to alter the formatter element.
  drupal_alter('pirobox_field_formatter_view_element', $element, $vars);

  return $element;
}

/**
 * Implements hook_theme().
 */
function pirobox_theme() {
  return array(
    'pirobox_formatter' => array(
      'variables' => array(
        'item' => NULL,
        'entity_type' => NULL,
        'entity' => NULL,
        'field' => NULL,
        'display_settings' => NULL,
        'cover_class' => NULL,
        'random' => FALSE,
        'entity_style_extra' => FALSE,
        'image_style_extra' => FALSE
      ),
      'file' => 'includes/pirobox.theme.inc'
    ),
    'pirobox_imagefield' => array(
      'variables' => array(
        'file' => NULL,
        'image' => NULL,
        'path' => NULL,
        'title' => NULL,
        'gid' => NULL
      ),
      'file' => 'includes/pirobox.theme.inc'
    ),
    'pirobox_link' => array(
      'variables' => array(
        'file' => NULL,
        'path' => NULL,
        'options' => NULL
      ),
      'file' => 'includes/pirobox.theme.inc'
    ),
  );
}

/**
 * Implements hook_pirobox_image_path_alter().
 */
function pirobox_pirobox_image_path_alter($uri) {
  global $base_url;

  // See http://drupal.org/node/1149910

  $scheme = file_uri_scheme($uri);

  if (variable_get('pirobox_relative_paths', FALSE) == TRUE && ($scheme == 'public' || $scheme == 'private')) {
    $base = $base_url . '/';
    $matches = array();

    if (preg_match('/^[\w\d]+:\/\/.*?\//', $base, $matches)) {
      $base = drupal_substr($matches[0], 0, -1);
      $uri = str_replace($base, '', file_create_url($uri));
    }
  }
}

/**
 * Machine name normally need to be unique but that does not apply to galleries.
 *
 * @return
 *   Always FALSE.
 *
 * @see pirobox_field_formatter_settings_form()
 */
function pirobox_gallery_exists() {
  return FALSE;
}

/**
 * Returns the Pirobox module supported entity types.
 *
 * @return
 *   The associative array is keyed with type machine name. Value is the
 *   translatable human name.
 */
function pirobox_supported_entity_types() {
  return array(
    'node' => t('Node'),
    'comment' => t('Comment'),
    'taxonomy_term' => t('Taxonomy term')
  );
}

/**
 * Returns human readable and translatable image style names.
 *
 * The returned style names are sanitized.
 *
 * @param $include_empty
 *   If TRUE a <none> option will be inserted in the options array.
 *
 * @return
 *   The associative array contains better redeable image style names.
 *
 * @see pirobox_field_formatter_settings_form_get_element()
 * @see pirobox_field_formatter_settings_summary_get_summary()
 */
function pirobox_image_style_options($include_empty = TRUE) {
  $style_options = image_style_options($include_empty);

  // Unset possible 'No defined styles' option.
  unset($style_options['']);

  foreach ($style_options as $style => $name) {
    $name = drupal_ucfirst($name);
    $name = preg_replace('/_|-/', ' ', $name);
    $style_options[$style] = t('@style-name', array('@style-name' => $name));
  }

  return $style_options;
}

/**
 * Return version of Pirobox and Blur gaussian jQuery plugin that is installed.
 *
 * @param $plugin
 *   A string contains the plugin name to get the version. Possible values:
 *     - pirobox
 *     - blur-gaussian
 *
 * @return
 *   A string contains the plugin version.
 *
 * @see pirobox.install, pirobox_requirements()
 */
function pirobox_get_version($plugin, $pirobox_js = NULL) {
  $version = 0;

  switch ($plugin) {
    case 'pirobox':
      $pattern = '#piroBox v\.([0-9\.a-z]+)#';

      // No file is passed in so use the default location.
      if (is_null($pirobox_js)) {
        $pirobox_js = pirobox_get_js($plugin);
      }

      // Return the version of Pirobox jQuery plugin, it it exists.
      if (file_exists($pirobox_js)) {
        $pirobox_plugin = file_get_contents($pirobox_js, NULL, NULL, 0, 34);
        if (preg_match($pattern, $pirobox_plugin, $matches)) {
          $version = $matches[1];
        }
      }
      break;

    case 'blur-gaussian':
      $pattern = '#blur-gaussian v\.([0-9\.a-z]+)#';

      // No file is passed in so use the default location.
      if (is_null($pirobox_js)) {
        $pirobox_js = pirobox_get_js($plugin);
      }

      // Return the version of Pirobox jQuery plugin, it it exists.
      if (file_exists($pirobox_js)) {
        $pirobox_plugin = file_get_contents($pirobox_js, NULL, NULL, 0, 38);
        if (preg_match($pattern, $pirobox_plugin, $matches)) {
          $version = $matches[1];
        }
      }
      break;
  }

  return $version;
}

/**
 * Return JS filenames of the Pirobox and Blur gaussian jQuery plugin.
 *
 * @param $library
 *   The string of library JS names to get. Possible values:
 *     - pirobox
 *     - blur-gaussian
 *
 * @return
 *   Boolean indicating if the JS is located.
 *
 * @see pirobox_get_version()
 */
function pirobox_get_js($library) {
  $library_path = pirobox_get_library_path($library);

  switch ($library) {
    case 'pirobox':
      if (file_exists($library_path . '/js/pirobox.js') && file_exists($library_path . '/js/pirobox.min.js')) {
        $pirobox_js_map = array('none' => 'pirobox.js', 'min' => 'pirobox.min.js');
        $pirobox_js = $pirobox_js_map[variable_get('pirobox_compression_type', 'min')];

        return $library_path . '/js/' . $pirobox_js;
      }
      break;
    case 'blur-gaussian':
      if (file_exists($library_path . '/blur-gaussian.js') && file_exists($library_path . '/blur-gaussian.min.js')) {
        $blur_gaussian_js_map = array('none' => 'blur-gaussian.js', 'min' => 'blur-gaussian.min.js');
        $blur_gaussian_js = $blur_gaussian_js_map[variable_get('pirobox_compression_type', 'min')];

        return $library_path . '/' . $blur_gaussian_js;
      }
      break;
  }
}

/**
 * Returns settings that are used by hook_field_formatter_info().
 *
 * @return
 *   An associative array contains default formatter settings.
 *
 * @see pirobox_field_formatter_info()
 * @see pirobox_media.module, pirobox_media_field_formatter_info()
 */
function pirobox_field_formatter_get_settings() {
  $settings = array(
    'pirobox_entity_style'     => 'none',
    'pirobox_image_style'      => '',
    'pirobox_gallery_grouping' => 'none',
    'pirobox_gallery_custom'   => '',
    'pirobox_gallery_random'   => FALSE,
    'pirobox_gallery_covering' => FALSE,
    'pirobox_caption'          => 'auto'
  );

  // Allow other modules to alter the formatter settings.
  drupal_alter('pirobox_field_formatter_get_settings', $settings);

  return $settings;
}

/**
 * Provide different form states properties.
 *
 * Possible situations are:
 *   - default: Core functionalities calls the settings form.
 *   - views: A views display call the settings form.
 *
 * @param $context
 *   A string to identify the form caller. Possible values:
 *     - default
 *     - views
 *
 * @return
 *   Associative array keyed with form element names.
 *
 * @see pirobox_field_formatter_settings_form_get_element()
 */
function _pirobox_formatter_settings_form_get_states($context) {
  $states = array();

  switch ($context) {
    case 'default':
      $states['pirobox_caption'] = array(
        'invisible' => array(
          ':input[name$="[settings_edit_form][settings][pirobox_entity_style]"]' => array('value' => 'hide')
        )
      );
      break;

    case 'views':
      $states['pirobox_caption'] = array(
        'invisible' => array(
          ':input[name$="[settings][pirobox_entity_style]"]' => array('value' => 'hide')
        )
      );
      break;
  }

  return $states;
}

/**
 * Provide a entity type information in Views module context.
 *
 * @return
 *   FALSE or a string contains the entity type.
 *
 * @see pirobox_field_formatter_settings_form_get_element()
 */
function _pirobox_get_entity_type_from_view($view, $field) {
  $entity_type = FALSE;

  switch ($view->base_table) {
    case 'node':
      $entity_type = $view->base_table;
      break;
    case 'comment':
      $entity_type = $view->base_table;
      break;
    case 'taxonomy_term_data':
      $entity_type = 'taxonomy_term';
      break;
  }

  if ($entity_type == FALSE) {
    if (isset($field['bundles']['node'])) {
      $entity_type = 'node';
    }
    elseif (isset($field['bundles']['comment'])) {
      $entity_type = 'comment';
    }
    elseif (isset($field['bundles']['taxonomy_term'])) {
      $entity_type = 'taxonomy_term';
    }
  }

  return $entity_type;
}


/**
 * Check if Pirobox should be active for the current URL.
 *
 * @return
 *   TRUE if Pirobox should be active for the current page.
 *
 * @see pirobox_init()
 */
function _pirobox_active() {
  // Make it possible deactivate Pirobox with parameter ?pirobox=no in the uri.
  if (isset($_GET['pirobox']) && $_GET['pirobox'] == 'no') {
    return FALSE;
  }

  // Code from the block_list funtion in block.module.
  $path = drupal_get_path_alias($_GET['q']);
  $pirobox_pages = variable_get('pirobox_pages', "admin*\nimg_assist*\nimce*\nnode/add/*\nnode/*/edit");
  // Compare with the internal and path alias (if any).
  $page_match = drupal_match_path($path, $pirobox_pages);

  if ($path != $_GET['q']) {
    $page_match = $page_match || drupal_match_path($_GET['q'], $pirobox_pages);
  }

  return !$page_match;
}

/**
 * Return caption options.
 *
 * The options differ depending on the entity type.
 *
 * @var $entity_type
 *   An string with supported values node, comment and taxonomy_term.
 *
 * @return
 *   An associative array contains caption options.
 *
 * @see pirobox_field_formatter_settings_form()
 * @see pirobox_field_formatter_settings_summary()
 * @see pirobox.admin.inc, pirobox_general_settings_form()
 */
function _pirobox_caption($entity_type) {
  $entity_title = t('Entity title');

  switch ($entity_type) {
    case 'node':
      $entity_title = t('Node title');
      break;
    case 'comment':
      $entity_title = t('Comment subject');
      break;
    case 'taxonomy_term':
      $entity_title = t('Term name');
      break;
  }

  return array(
    'auto'         => t('Automatic'),
    'title'        => t('Image title text'),
    'alt'          => t('Image alt text'),
    'entity_title' => $entity_title,
    'none'         => t('None (filename)')
  );
}

/**
 * Returns template/style informations from the installed Pirobox jQuery plugin.
 *
 * Scan available CSS directories from the Pirobox jQuery plugin to get
 * informations.
 *
 * @return
 *   The object Contains file uri, file name, style path, style machine name
 *   and the style human name.
 *
 * @see pirobox.admin.inc, pirobox_general_settings_form()
 * @see pirobox.admin.inc, _pirobox_admin_settings_check_plugin_path()
 */
function _pirobox_get_styles() {
  $scan = array();
  $library_path = pirobox_get_library_path('pirobox');
  $scan = file_scan_directory($library_path . '/css', '/piroboxstyle.css/');

  foreach ($scan as $dirs => $dir) {
    $scan[$dir->uri]->path = drupal_dirname($dir->uri);
    $machinename = preg_replace("/^.*\//", '', $scan[$dir->uri]->path);
    $scan[$dir->uri]->template_machinename = $machinename;

    $humanname = preg_replace("/demo/", t('Style') . ' ', $machinename);
    $humanname = preg_replace("/_|-/", ' ', $humanname);
    $scan[$dir->uri]->template_humanname = $humanname;
  }

  return array_reverse($scan);
}

/**
 * Returns the option to use the JS files.
 *
 * @return
 *   An assiociative array contains the JS compression options.
 *
 * @see pirobox.admin.inc, pirobox_general_settings_form()
 */
function _pirobox_get_js_options() {
  return array(
    'min' => t('Production (Minified)'),
    'none' => t('Development (Uncompressed Code)')
  );
}

/**
 * Returns overlay background images.
 *
 * @return
 *   An associative array keyed with image path. Value is the image name.
 *
 * @see pirobox.admin.inc, pirobox_general_settings_form()
 * @see pirobox.admin.inc, _pirobox_admin_settings_check_plugin_path()
 */
function _pirobox_get_background_images() {
  $scan = array();
  // Use dummy to prevent FAPI error are no images found.
  $images = array('none' => FALSE);

  $scan = file_scan_directory(pirobox_get_library_path('pirobox') . '/images-background', '/.*/', array('key' => 'name'));
  foreach ($scan as $image) {
    $validate = file_validate_extensions($image, 'png gif jpg jpeg');
    if (!isset($validate[0])) {
      $images[$image->uri] = $image->name;
    }
  }

  // Exists at least one allowed image remove the dummy.
  if (count($images) > 1) {
    unset($images['none']);
  }

  return $images;
}

/**
 * Returns a gallery covering message.
 *
 * @return
 *   The string contains the the gallery covering information.
 *
 * @see pirobox_field_widget_image_image_form_alter()
 */
function _pirobox_get_covering_message() {
  return t('Pirobox <em>gallery covering</em> is active. Some display-variants of content show only the first image. The Pirobox lightbox displays more images.');
}

/**
 * Randomize a associative array.
 *
 * @param $array
 *   The array to randomize.
 *
 * @return
 *   Randomized associative array.
 *
 * @see pirobox_field_formatter_view()
 */
function _pirobox_randomize_assoc_array($array) {
  if (!is_array($array)) {
    return;
  }
  else {
    $size_of = count($array);
    $return = array();
    $rand_array = array();

    // Build our random number array.
    for ($j = 0; $j < $size_of; ++$j) {
     $rand_array[] = $j;
    }
    // Shuffle that order for new keys.
    shuffle($rand_array);
    // Build the new associative array.
    for($i = 0; $i < $size_of; ++$i) {
      $return[$i] = $array[$rand_array[$i]];
    }
  }

  return $return;
}
